## Data Definition Language (SQL-DDL)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-ddl-basics</span>

> **tl/dr;** _SQL besteht aus mehreren Teilsprachen, die sich mit der Datenbankstruktur (DDL), dem Datenbankinhalt (DML) und den Berechtigungen (DCL) beschäftigen. In diesem Artikel soll es zunächst um die Grundlagen der Strutur gehen, die in der Data Definition Language (SQL-DDL) beschrieben wird._

### Einstieg in die  _**D**ata **D**efinition **L**anguage_ (DDL)

#### Struktureinheiten in der DDL

Die meisten Datenbankmanagementsysteme gliedern die Daten in unterschiedlichen Strukturen:

- **Datenbanken** sind in der Regel die oberste Struktur, in der alles Weitere organisiert ist.

- **Schema** sind Einheiten, die mehrere unterschiedliche Tabellen zusammenfasst. Sie bilden Namensräume, innerhalb derer Bezeichner eindeutig sind. Nicht alle Datenbankmanagementsysteme arbeiten mit Schema.

- **Tabellen** schließlich sind die Struktur, die die eigentliche Datenmenge speichert (die Datensätze, die wir in Entitätstypen und Relationen gegliedert hatten).

Für jede dieser Einheiten stellt die DDL Befehle zum Erstellen, Lesen, Ändern und Löschen bereit. Diese Abfolge (**C**reate, **R**ead, **U**pdate, **D**elete - kurz C.R.U.D.) wird uns im Verlauf der Datenbankarbeit noch häufiger begegnen.

#### Die Sache mit dem Standard und der Realität...

SQL ist ein offizieller ISO/IEC-Standard, der die Grammatik von SQL-Statements festlegen soll (aktuell: ISO/IEC 9075-1:2016 ^[hier kann ISO/IEC 9075-1:2016 für knapp 200€ erworben werden: [https://www.iso.org/standard/63555.html](https://www.iso.org/standard/63555.html)]). Eine frühe Version dieses Standards, `SQL-92` bildet die Basis der Abfragesprachen aller relationalen Datenbankmanagementsysteme. Über diese Basis hinaus hat jedes System seine eigenen Sprachbestandteile entwickelt, die in der Struktur ähnlich, in der konkreten Ausprägung jedoch unterschiedlich sind. Es gibt große Überschneidungen bei trivialen SQL-Abfragen, aber im Detail unterscheiden sich die SQL-Implementierungen erheblich.

Ich werde versuchen auf die ein oder andere Implementierungseigenheit einzugehen: die unterschiedlichen Datentypen, unterschiedliche Bezeichnungen usw. Daher habe ich in einigen Bereichen Tabellen, in denen die verbreitetsten Dialekte einzeln aufgeführt sind. Ich werde Stück für Stück versuchen, diese Tabellen zu füllen - zu Beginn werden aber viele Zellen noch leer bleiben. Leere Zellen im Folgenden heißen also nicht, dass der entsprechende Befehl in diesem SQL-Dialekt nicht vorhanden ist. Wer mag, kann mir gerne dabei helfen, die entsprechenden Lücken zu füllen! Ich habe entschieden, die folgenden Datenbankmanagement-Systeme ins Augenmerk zu nehmen: MS SQL-Server (T-SQL), MySQL / MariaDB, postgreSQL, OracleDB, SQLite.


### Datenbanken verwalten

Die übergeordnete Struktureinheit in DBMS sind Datenbanken. In der Regel ist es sinnvoll, nicht die Standarddatenbank des DBMS für eigene Tabellen zu nutzen - daher muss eine eigene Datenbank erzeugt und verwaltet werden.

#### Datenbank erzeugen

Die Erzeugung von Datenbanken erfolgt in den meisten DBMS noch sehr nah am Standard.

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
CREATE DATABASE datenbankname;
```

Wenn sie nur erzeugt werden soll, falls sie nicht bereits existiert:

```sql
CREATE DATABASE IF NOT EXISTS datenbankname;
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
CREATE DATABASE datenbankname;
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
CREATE DATABASE datenbankname;
```

_postgreSQL_: Innerhalb einer Verbindung können keine `Databases` erzeugt werden, da diese jeweils bei der Connection übergeben werden.

</span>

#### Datenbank auswählen

```sql
USE datenbankname;
```

#### Vorhandene Datenbanken anzeigen

| |MSSQL|MySQL / MariaDB|postgreSQL|OracleDB|SQLite|
|---|---|---|---|---|---|
|  | `SELECT Name from sys.Databases`  | `SHOW DATABASES;` |`SELECT datname FROM pg_database;` | | |

#### Datenbanken anpassen

Die meisten Eigenschaften einer Datenbank lassen sich auch im Nachgang noch anpassen. Das Schlüsselwort dazu lautet `ALTER DATABASE`:

```sql
ALTER DATABASE datenbankname MODIFY NAME = "neuerDatenbankName"
```

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb">

```sql
ALTER DATABASE dietestdb CHARACTER SET latin1 COLLATE latin1_swedish_ci;
```

MySQL verfügt über mehrere unterschiedliche Tabellenengines - beispielsweise war früher die Engine MyISAM voreingestellt, heute ist es i.d.R: InnoDB. Per DDL-Befehl kann zwischen unterschiedlichen Engines gewechselt werden:

```sql
ALTER TABLE `dietestdb`.`mp3sammlung` ENGINE = InnoDB;
```
</span>

#### Datenbank löschen


```sql
DROP DATABASE datenbankname;
```

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

Um in SQL-Server zu prüfen, ob die Datenbank, die man löschen will, überhaupt existiert, kann der folgende Befehl genutzt werden:

```sql
IF EXISTS(SELECT * FROM sys.databases WHERE name = 'datenbankname')
  BEGIN
  DROP DATABASE datenbankname
  END
GO
```

</span>

### Namensräume in Schema verwalten

Nicht jedes DBMS nutzt Schema zur Gliederung von Namensräumen. Einige nutzen es, ohne, dass man es merkt. Bei anderen wundert man sich, dass in jedem Befehl eine vermeintlich nutzlose Zeichenkette mitgegeben wird. So wird in MS SQL-Server beispielsweise immer das Standardschema `dbo` in Befehlen angegeben.

#### Neues Schema erzeugen

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb">

MariaDB verwendet keine Schema und nutzt die Begriffe `DATABASE` und `SCHEMA` in Befehlen synonym.

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
CREATE SCHEMA meinSchemaName
```

Um in SQL-Server zu prüfen, ob ein Schema, dass man erzeugen will, nicht bereits existiert, kann der folgende Befehl genutzt werden:

```sql
IF NOT EXISTS ( SELECT *
 FROM sys.schemas
 WHERE name = N'meinSchemaName' )
 EXEC('CREATE SCHEMA meinSchemaName AUTHORIZATION [dbo]');
GO
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
CREATE SCHEMA IF NOT EXISTS meinSchemaName;
```

</span>

#### Vorhandene Datenbanken anzeigen

| |MSSQL|MySQL / MariaDB|postgreSQL|OracleDB|SQLite|
|---|---|---|---|---|---|
|  | `SELECT Name from sys.Schemas`  |  | | | |


### Tabellen bearbeiten

#### Namensgebung

Die einzelnen DBMS haben jeweils eigene Namensregeln. Eine sinnvolle Namensregel lautet etwa:

- Erstes Zeichen muss ein Buchstabe sein, danach bis zu 29 Zeichen, Groß- oder Kleinbuchstaben, Zahlen, Underscore, Dollar oder Raute, als RegEx: `[A-Za-z] [A-Za-z0-9_\$\#]{0,29}`.

- In der Regel behandeln die DBMS Tabellennamen nicht _casesensitive_, sofern das DBMS jedoch in einem Dateisystem abgespeichert wird, dass _casesensitive_ ist, kann es zu Komplikationen führen. Daher: besser nur Kleinbuchstaben verwenden.

- Tabellennamen sollte im Plural gehalten sein (Konvention, nicht Dogma).

#### Tabellen erzeugen

```sql
CREATE TABLE IF NOT EXISTS datenbankname.tabellenname (attribut1 datentyp1, attribut2 datetyp2, ...);
```

![Railroad-Diagramm für CREATE TABLE](images/sql-ddl-railroad.png)

Neben den Datentypen ([siehe Datentyps-Übersicht](https://oer-informatik.gitlab.io/db-sql/ddl-basics/Datentypen.html)) können beim Erzeugen / Anpassen von Tabellen auch Constraints festgelegt werden. Details finden sich auf der gesonderten [Übersicht zu Constraints](https://oer-informatik.gitlab.io/db-sql/ddl-basics/Constraint.html).

#### Tabellenstruktur anpassen

```sql
ALTER TABLE ...
```

Tabellen verändern:

```sql
ALTER TABLE tabellenName ADD neuesAttribut VARCHAR(50)
```

In MySQL kann zusätzlich die Position des neuen Attributs bestimmt werden, in dem `AFTER vorgaengerAttribut` ergänzt wird.

Datentyp eines Merkmals ändern:

```sql
ALTER TABLE tabellenName MODIFY attributName VARCHAR(128);
```

Merkmale umbenennen:

```sql
ALTER TABLE tabellenName CHANGE alterAttributName neuerAttributName VARCHAR(128);
```

Merkmale löschen:

```sql
ALTER TABLE tabellenName DROP zuLoeschendesAttribut;
```

#### Tabellenname anpassen

```sql
RENAME TABLE alterName TO neuerName;
```

#### Tabellen anzeigen

| | MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| | `SELECT Name from sys.Tables`  | `SHOW TABLES;` | | | |


#### Tabellenstruktur anzeigen

In MySQL/MariaDB kann man die Tabellenstruktur per Abfrage ausgeben lassen:
```sql
DESCRIBE tabellenname;
```

#### Tabellen löschen

```sql
DROP TABLE ...
```
### Allgemeine Infos zum Tabellenerstellen

Um Tabellen passgenau erstellen zu können, benötigen wir Wissen über die verfügbaren Grundlagen zum Tabellen- und Datenbankerstellen, die Datentypen sowie die _Constraints_ (wie Schlüsselattribute, Eingabepflicht usw.).

- [Hier findet sich der Artikel zur Datentypsübersicht](https://oer-informatik.de/sql-ddl-datentypen)

- [Hier findet sich der Artikel zur Constraints](https://oer-informatik.de/sql-ddl-constraint)

- [Hier findet sich der Artikel zur Grundlagen der DDL](https://oer-informatik.de/sql-ddl-basics)

### Links und weitere Informationen

- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)

