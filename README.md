# Installation von Datenbankmanagementsystemen

* [SQL-DDL Befehle](https://oer-informatik.gitlab.io/db-sql/ddl-basics/DDL.html) [(als PDF)](https://oer-informatik.gitlab.io/db-sql/ddl-basics/DDL.pdf)
* [SQL-Datentypen](https://oer-informatik.gitlab.io/db-sql/ddl-basics/Datentypen.html) [(als PDF)](https://oer-informatik.gitlab.io/db-sql/ddl-basics/Datentypen.pdf)
* [SQL-Constraints](https://oer-informatik.gitlab.io/db-sql/ddl-basics/Constraint.html) [(als PDF)](hhttps://oer-informatik.gitlab.io/db-sql/ddl-basics/Constraint.pdf)

# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
