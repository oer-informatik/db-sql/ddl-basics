## _Constraints_ in der Data Definition Language (SQL-DDL)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-ddl-constraints</span>

> **tl/dr;** _Die Informationen, ob es sich bei einem Attribut um ein Schlüsselattribut handelt, ob es eingabepflichtig oder einzigartig ist: all das wird über Constraints festgelegt. Es gibt unterschiedliche Möglichkeiten, Constraints festzulegen. Einige werden hier kurz vorgestellt._

### DBMS-einheitliche _Constraint_-Definitionen


![Railroad-Diagramm für CREATE TABLE](images/sql-ddl-create-railroad.png)

|Art der Nebenbedingung| Inline |named constraint |
| -------- | -------- | -------- |
| Primärschlüssel |```PRIMARY KEY (field)``` | ```PRIMARY KEY (field)``` |
| zusammengesetzte Primärschlüssel |  | ```CONSTRAINT (NameDesConstraints) PRIMARY KEY (field1, field2)``` |
| Fremdschlüssel |```FOREIGN KEY (FK_Attribut) REFFERENCES Ref_Tabelle(Ref_Attribut)```<br/>[```ON DELET```E ...] [```ON UPDATE``` ...]| ```CONSTRAINT (NameDesConstraints) FOREIGN KEY (FK_Attribut) REFFERENCES Ref_Tabelle(Ref_Attribut)```<br/>[```ON DELETE``` ...] [```ON UPDATE``` ...]|
| einzigartig|```UNIQUE``` | |
| eingabepflichtig|```NOT NULL``` |  |
| optional| standardmäßig (explizit in manchen DBMS: `NULL`) |  |
| Eingabe von Default-Werten (z.B. immer 0 bei Zahlen)     |```DEFAULT value``` |  |
| Check-Constraints (nur eingegrenzte Werte erlauben)     | | ```CONSTRAINT volljaehrig  CHECK (Alter>=18)``` |

Die Constraints `PRIMARY KEY`, `FOREIGN KEY`, `UNIQUE` und `CHECK` lassen sich in den meisten DBMS als _named constraint_ formulieren. Die zugrunde liegende Syntax findet sich in folgendem Railroad-Diagramm wieder:

![Railroad-Diagramm für _named constraints_](images/sql-ddl-constraints-railroad.png)

### DBMS-spezifische Constraint-Notationen:

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
| Automatisch hochzählenden Indizes|```IDENTITY```  | ``` AUTO_INCREMENT```  | ```SERIAL```  |``` SERIAL```  | ``` AUTOINCREMENT```
| offizielle Dokumentation des DBMS zu Constraints|  [...](https://link.to)   |     [MySQL Constraints](https://dev.mysql.com/doc/refman/5.6/en/create-table-foreign-keys.html)   [MariaDB Constraints](https://mariadb.com/kb/en/constraint/)   | [postgres-Doku](https://www.postgresql.org/docs/current/ddl-constraints.html) ||[SQLite-Doku](https://www.sqlite.org/syntax/table-constraint.html)

### Nachträgliches Ergänzen von _Constraints_

Einen Constraint im Nachhinein anfügen (als _named constraint_):

```sql
ALTER TABLE personen ADD CONSTRAINT volljaehrig  CHECK (Alter>=18);
```

Named Constraints im Nachhinein löschen:

```sql
ALTER TABLE personen DROP CONSTRAINT volljaehrig;
```

### Allgemeine Infos zum Tabellenerstellen

Um Tabellen passgenau erstellen zu können, benötigen wir Wissen über die verfügbaren Grundlagen zum Tabellen- und Datenbankerstellen, die Datentypen sowie die _Constraints_ (wie Schlüsselattribute, Eingabepflicht usw.).

- [Hier findet sich der Artikel zur Datentypsübersicht](https://oer-informatik.de/sql-ddl-datentypen)

- [Hier findet sich der Artikel zur Constraints](https://oer-informatik.de/sql-ddl-constraint)

- [Hier findet sich der Artikel zur Grundlagen der DDL](https://oer-informatik.de/sql-ddl-basics)

### Links und weitere Informationen

- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)

