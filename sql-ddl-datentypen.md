## Datentypen in der Data Definition Language (SQL-DDL)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-ddl-datentypen</span>

> **tl/dr;** _Alle Datenbankmanagemensysteme verfügen über ein ähnliches Set an Datentypen. Aber leider heißen sie oft anders und haben einen geringfügig anderen Funktionsumfang. Ich habe hier versucht für die wichtigsten DBMS Namen der Standardtypen zusammenzustellen, für Detailunterschiede muss ich aber an die jeweiligen Dokumentationen der DBMS verweisen._


### DBMS-spezifische Datentypen

Am einfachsten werden Datentypen deutlich, wenn dargestellt wird, was man damit speichern kann. Ausgangsbeispiel soll eine Tabelle `kunden` sein, in der allerlei Infos zu Kund*innen gespeichert werden; manches davon -zugegeben- etwas an den Haaren herbeigezogen:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="Oracle" onclick="openTabsByDataAttr('oracle', 'sql')">Oracle</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="sqlite" onclick="openTabsByDataAttr('sqlite', 'sql')">SQLite</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
CREATE TABLE kunden (
    user_id  INT PRIMARY KEY AUTO_INCREMENT, /* Fortlaufende ID, Primärschlüssel*/ 
    lastname CHAR(32) NOT NULL,              /* Zeichenkette, eingabepflichtig, performanceoptimiert*/
    firstname CHAR(32),                      /* Zeichenkette, performanceoptimiert*/ 
    street VARCHAR(64),                       /* Zeichenkette, speicheroptimiert*/
    birthday DATE,                            /* Datum */
    lastchanged DATETIME,                     /* Datums- und Zeitwert */
    height DECIMAL(19,2),                     /* Festkommawert mit 19 Stellen, davon 2 Nachkommastellen */
    income DECIMAL(19,4),                     /* Festkommawert 4 Nachkommastellen, 8 Byte */
    isActive TINYINT(1),                      /* Boolean-Wert (true/false) */
	atomweight DOUBLE,                        /* 64 bit Gleitkommawert */
    temperature FLOAT                         /* 32 bit Gleitkommawert */
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
CREATE TABLE dbo.kunden (
    user_id int IDENTITY(1,1) PRIMARY KEY, /* Fortlaufende ID, Primärschlüssel*/ 
    lastname char(32) NOT NULL,            /* Zeichenkette, eingabepflichtig, performanceoptimiert*/
    firstname char(32),                    /* Zeichenkette, performanceoptimiert*/ 
    street varchar(64),                    /* Zeichenkette, speicheroptimiert*/
    birthday date,                         /* Datum */
    lastchanged datetime,                  /* Datums- und Zeitwert */
    height decimal(19,2),                  /* Festkommawert mit 19 Stellen, davon 2 Nachkommastellen */
    income money,                          /* Festkommawert 4 Nachkommastellen, 8 Byte */
    isActive bit,                          /* Boolean-Wert (true/false) */
	atomweight float,                      /* 64 bit Gleitkommawert */
    temperature real,                      /* 32 bit Gleitkommawert */
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
CREATE TABLE kunden (
    user_id serial PRIMARY KEY,  /* Fortlaufende ID, Primärschlüssel*/ 
    lastname character(32) NOT NULL,  /* Zeichenkette, eingabepflichtig, performanceoptimiert*/
    firstname character(32),          /* Zeichenkette, performanceoptimiert*/ 
    street character varying(64),          /* Zeichenkette, speicheroptimiert*/
    birthday date,               /* Datum */
    lastchanged timestamp,       /* Datums- und Zeitwert */
    height numeric(19,2),        /* Festkommawert mit 19 Stellen, davon 2 Nachkommastellen */
    income numeric(19,4),        /* Festkommawert 4 Nachkommastellen, 8 Byte */
    isActive boolean,            /* Boolean-Wert (true/false) */
	atomweight double precision, /* 64 bit Gleitkommawert */
    temperature real             /* 32 bit Gleitkommawert */
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="oracle" style="display:none">

Noch keine Beispieldaten für Oracle.

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="sqlite" style="display:none">

Noch keine Beispieldaten für SQLite.

</span>

Ein paar Untschiede fallen schnell auf, wenn man durch die Tabs oben klickt: auch wenn SQL-Interpreter in der Regel case-insensitive sind, nutzen einige DBMS Kleinbuchstaben (MS-SQL, postgres), während andere die Datentypen großschreiben (MariaDB, MySQL).

Die Dokumentationen für Datentypen der einzelnen DBMS findet sich hier:


| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| offizielle Dokumentation des DBMS zu Datentypen|  [Microsoft SQL Server Datentypen](https://docs.microsoft.com/de-de/sql/t-sql/data-types/data-types-transact-sql)   |     [MySQL - Data Types](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)   /<br/>/<br/>   [MariaDB - Data Types](https://mariadb.com/kb/en/data-types/)|[postgresql Doku](https://www.postgresql.org/docs/current/datatype.html) |[Oracle8 Application Developer's Guide](https://docs.oracle.com/cd/A58617_01/server.804/a58241/ch5.htm#417949)   | [SQLite Datatypes](https://www.sqlite.org/datatype3.html)|

#### Zahlentypen

Für unterschiedliche Anwendungsfälle werden die Daten mit unterschiedlicher Präzision und Größenordnung abgespeichert. Die verbreitetsten Gruppen sind:


##### Ganzzahlen

Ganzzahlen können präzise auf Gleichheit geprüft werden und bilden unterschiedliche Zahlenräume ab: je nachdem, ob sie vorzeichenbehaftet (`signed`) oder ohne Vorzeichen (`unsigned`) sind und ob ihre Datenbreite 32 Bit oder 64 Bit sind, sind Zahlen im Rahmen von:

||`signed`|`unsigned`|
|---|---|---|
|8 Bit<br/>1 Byte|$$0 .. (2^{8}-1)$$<br/>$$0..255$$|$$-(2^{8-1}) .. (2^{8-1}-1)$$<br/>$$-128..127$$|
|16 Bit<br/>2 Byte|$$0 .. (2^{16}-1)$$<br/>$$0..65535$$|$$-(2^{16-1}) .. (2^{16-1}-1)$$<br/>$$-32.768..32.767$$|
|32 Bit<br/>4 Byte|$$0 .. (2^{32}-1)$$<br/>$$0..4.294.967.295$$|$$-(2^{32-1}) .. (2^{32-1}-1)$$<br/>$$-2.147.483.648..2.147.483.647$$|
|64 Bit<br/>8 Byte|$$0 .. (2^{64}-1)$$<br>$$0..18.446.744.073.709.551.615$$|$$(-2^{64-1}) .. (2^{64-1}-1)$$|
|n Bit|$0 .. (2^{n}-1)$|$(-2^{n-1}) .. (2^{n-1}-1)$|

Im ANSI-SQL-Standard sind Ganzzahlen mit 2 Byte (`SMALLINT`) und 4 Byte (`INTEGER`) definiert - viele DBMS setzen diese auch um. Leider heißen die Datentypen in vielen DBMS unterschiedlich. Oft gibt es jedoch interne Aliasse, die Standardbezeichnungen wie "INTEGER" dann in den korrekten Datentyp umsetzen.

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Wahrheitswerte|`bit` | `TINYINT(1)` / <br/>`BOOLEAN` |  `boolean` |`NUMBER`| `Numeric`|
| Ganzzahl 32Bit    |`int` |`INT`  |`integer` | `NUMBER(38)` ^[Die Datenbreite in Byte wird in Klammern angegeben, per default sind es 8Byte (64Bit).] ||
| Ganzzahl 64Bit   | `bigint`|`BIGINT`  | `bigint` | `NUMBER(38)` ^[Die Datenbreite in Byte wird in Klammern angegeben, per default sind es 8Byte (64Bit).] |`Integer` ^[SQLite: Die Speicherbreite von Integer-Werten wird dynamisch an Hand der Größe der Werte vergeben.]|

Es gibt aber noch eine Reihe weiterer Datentypen - hier lohnt ein Blick in die jeweilige Dokumentation.

##### Festkommazahlen

Festkommazahlen teilen mit Ganzzahlen die Eigenschaft, dass sie präzise verglichen werden können. Im Rahmen der Präsision kann die Gleichheit zweier Festkommazahlen nachgewiesen werden. Festkommazahlen werden v.a. im Rahmen der Berechnungen mit Währungen genutzt, einige DBMS bieten daher gesonderte Datentypen an, die auch Währungsinformationen enthalten. Die verbreiteten Datentypen in diesem Kontext sind:

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Festkommazahlen<br/> und Datentypen <br/>für €-Währungen    |`decimal(19,2)`<br/>`money`, `smallmoney` | `DECIMAL(precision, scale)` / `NUMERIC(precision, scale)` | `money` | `NUMBER` | `Numeric`|


##### Gleitkommazahlen

Gleitkommazahlen haben den Vorzug, dass sie unabhängig von der Größenordnung immer die gleiche Anzahl an Nachkommastellen Präzision bieten. Zudem decken Sie einen sehr großen Größenordnungsbereich ab. Allerdings kann man sie nicht ohne weitere Vorkehrungen auf Gleichheit überprüfen.

Analog zu den meisten Programmiersprachen bilden DBMS in der Regel 32-Bit (Float) und 64-Bit (Double) Gleitkommazahlen ab. Dabei nutzen Sie die Definitionen für Gleitkommazahlen, die in der Norm  IEEE-754 festgeschrieben sind^[Hintergrund zu Gleitkommazahlen findet sich z.B. in [diesem Wikipedia-Artikel](https://de.wikipedia.org/wiki/IEEE_754)]:

||`MIN`<br/> kleinster Betrag ungleich 0|`MAX`<br/> größter und kleinster <br/>(mit neg. Vorzeichen) Betrag|
|---|---|---|
|32 Bit|$$1.175494 \cdot 10^{-38}$$|$$3.402823 \cdot 10^{38}$$|
|64 Bit|$$4.94066 10^{−324}$$|$$1.79769 \cdot 10^{308}$$|

Auch hier ist es so, dass jedes DBMS die Bezeichnung der 

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Gleitkommazahl 32Bit     |`real` / `float(24)`  | `FLOAT` | `Real` | `REAL` |`Real`|
| Gleitkommazahl 64Bit     |`float` / `float(53)` ^[MS-SQL: für die Gleitkommazahlen ist float() der Standarddatentyp, wobei die Länge der Mantisse in Klammer übergeben wird. float(24) entspricht 32Bit Gleitkommazahlen nach ISO-Standard IEEE 754 und wird auch mit real abgekürzt. float() ist per default float(53), was dem 64-Bit ISO-Wert entspricht.] | `DOUBLE` | `double precision` | `FLOAT` |`Real`|


#### Zeichenketten

Es gibt drei wesentliche Gruppen von Zeichenketten-Datentypen:

- performanceoptimierte (`CHAR`): hier wird für jeden Datensatz eine feste Breite Speicher reserviert - dadurch ist leicht berechenbar, an welcher Stelle sich die gesuchte Information befindet: gute Performance auf Kosten des Speicherbedarfs.

- speicheroptimiert (`VARCHAR`): Jede Zeichenkette belegt nur soviel Platz, wie sie gerade braucht. Das ist sehr speichersparend, hat aber zur Folge, dass jedes Mal berechnet werden muss, wo sich die Information befindet. Das kostet Performance, schont aber den Speicherbedarf.

- nicht durchsuchbar: einige DBMS bieten Texttypen für sehr lange Texte, die jedoch nicht mehr durchsuchbar und indizierbar sind. Häufig werden diese Datentypen für Text der länger als 256 Zeichen ist verwendet.

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Zeichenketten performanceoptimiert<br/>Schneller Zugriff durch definierte Speicherbereiche|`char` / `nchar` | `CHAR` / `BINARY` | `character` | `CHAR` | `Text`|
| Zeichenketten speicheroptimiert<br/>Weniger Speicherplatz, dafür muss Position berechnet werden|`varchar` / `nvarchar` | `VARCHAR` / `VARBINARY` |  character varying |`VARCHAR`|`Text`|
| sehr SEHR lange Texte|`text` / `ntext` | `LONGTEXT` |  `text` |`LONG`| `Real`|


#### Datums- und Zeitwerte

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Datum|`date` | `DATE` | `date` | `DATE` | `Numeric`|
| Zeit|`time` | `TIME` |  `time` | `DATE` | `Numeric`|
| Datum + Zeit|`datetime`|  `DATETIME` / `TIMESTAMP` | `timestamp` |`TIMESTAMP`| `Numeric`|

#### Dateien, Binärdaten, XML, JSON

| Datentyp| MSSQL |MySQL / MariaDB | postgreSQL  | OracleDB | SQLite|
|---|---|---|---|---|---|
| Dateien (Binärdaten)|`image` / `BLOB` | `BLOB` |  `bytea` | `BLOB` | `BLOB`|
| JSON|(`nVarChar`) | `JSON` |  `json` | `VARCHAR2`, `CLOB`, und `BLOB` | `Text`|

### Allgemeine Infos zum Tabellenerstellen

Um Tabellen passgenau erstellen zu können, benötigen wir Wissen über die verfügbaren Grundlagen zum Tabellen- und Datenbankerstellen, die Datentypen sowie die _Constraints_ (wie Schlüsselattribute, Eingabepflicht usw.).

- [Hier findet sich der Artikel zur Datentypsübersicht](https://oer-informatik.de/sql-ddl-datentypen)

- [Hier findet sich der Artikel zur Constraints](https://oer-informatik.de/sql-ddl-constraint)

- [Hier findet sich der Artikel zur Grundlagen der DDL](https://oer-informatik.de/sql-ddl-basics)

### Links und weitere Informationen
- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)
